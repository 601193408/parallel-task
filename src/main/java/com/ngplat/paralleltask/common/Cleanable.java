/**
 * @project: parallel-task
 * @package: com.ngplat.paralleltask.common
 * @filename: Cleanable.java
 *
 * Copyright (c) 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 */
package com.ngplat.paralleltask.common;

/**
 * @typename: Cleanable
 * @brief: 资源清理, 初始化
 * @author: KI ZCQ
 * @date: 2018年5月24日 下午10:29:37
 * @version: 1.0.0
 * @since
 * 
 */
public interface Cleanable {
	
	/**
	 * 
	 * @Description: 资源清理, 初始化
	 */
	public abstract void cleanResource();
	
}
