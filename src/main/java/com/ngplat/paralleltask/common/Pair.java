/**
 * @project: parallel-task
 * @package: com.ngplat.paralleltask.common
 * @filename: Pair.java
 *
 * Copyright (c) 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 */
package com.ngplat.paralleltask.common;

/** 
 * @typename: Pair
 * @brief: 一对通用对象组合
 * @author: KI ZCQ
 * @date: 2018年5月15日 上午10:45:55
 * @version: 1.0.0
 * @since
 * 
 */
public class Pair<T1, T2> {

	private T1 info;
	private T2 processor;

    public Pair() {
    }

    /**
     * 创建一个新的实例 Pair.
     * @param info
     * @param processor
     */
    public Pair(T1 info, T2 processor) {
        this.info = info;
        this.processor = processor;
    }

    /**
     * @Description: 构造一个Pair 
     * @param info
     * @param processor
     * @return
     */
    public static <T1, T2> Pair<T1, T2> wrapPair(T1 info, T2 processor) {
        return new Pair<T1, T2>(info, processor);
    }
    
    /**
	 * info
	 *
	 * @return  the info
	 * @since   1.0.0
	*/
	public T1 getInfo() {
		return info;
	}

	/**
	 * @param info the info to set
	 */
	public void setInfo(T1 info) {
		this.info = info;
	}

	/**
	 * processor
	 *
	 * @return  the processor
	 * @since   1.0.0
	*/
	public T2 getProcessor() {
		return processor;
	}

	/**
	 * @param processor the processor to set
	 */
	public void setProcessor(T2 processor) {
		this.processor = processor;
	}
	
}
