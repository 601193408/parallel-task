/**
 * @project: parallel-task
 * @package: com.ngplat.paralleltask.model
 * @filename: JobInfo.java
 *
 * Copyright (c) 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 */
package com.ngplat.paralleltask.model;

import com.ngplat.paralleltask.task.TaskPool;

/**
 * @typename: JobInfo
 * @brief: 作业信息
 * @author: KI ZCQ
 * @date: 2018年5月24日 上午11:17:08
 * @version: 1.0.0
 * @since
 * 
 */
public class JobInfo {
	
    public static final String DEFAULT_GROUP_ID = "DEFAULT_JOB_ID";
    public static final String DEFAULT_GROUP_NAME = "DEFAULT_JOB_NAME";
	
	// 作业Id
	private String jobId;
	// 作业名字
	private String jobName;
	// 任务节点
	private TaskPool taskPool;
	
	/**
	 * 创建一个新的实例 JobInfo.
	 * @param id
	 */
	public JobInfo(String id) {
		this(id, null);
    }

	/**
	 * 创建一个新的实例 JobInfo.
	 * @param id
	 * @param name
	 */
    public JobInfo(String id, String name) {
    	if(id != null)
            this.jobId = id;
        else
            this.jobId = DEFAULT_GROUP_ID;
    	
        if(name != null)
            this.jobName = name;
        else
            this.jobName = DEFAULT_GROUP_NAME;
        
        // jobId不可为null
        taskPool = new TaskPool(jobId);
    }

	/**
	 * jobId
	 *
	 * @return  the jobId
	 * @since   1.0.0
	*/
	public String getJobId() {
		return jobId;
	}

	/**
	 * @param jobId the jobId to set
	 */
	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	/**
	 * jobName
	 *
	 * @return  the jobName
	 * @since   1.0.0
	*/
	public String getJobName() {
		return jobName;
	}

	/**
	 * @param jobName the jobName to set
	 */
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	/**
	 * taskPool
	 *
	 * @return  the taskPool
	 * @since   1.0.0
	*/
	public TaskPool getTaskPool() {
		return taskPool;
	}

	/**
	 * @param taskPool the taskPool to set
	 */
	public void setTaskPool(TaskPool taskPool) {
		this.taskPool = taskPool;
	}
    
}
