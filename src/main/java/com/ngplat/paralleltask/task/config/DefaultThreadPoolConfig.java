/**
 * @project: parallel-task
 * @package: com.ngplat.paralleltask.task.config
 * @filename: DefaultThreadPoolConfig.java
 *
 * Copyright (c) 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 */
package com.ngplat.paralleltask.task.config;

/** 
 * @typename: DefaultThreadPoolConfig
 * @brief: 默认线程池配置
 * @author: KI ZCQ
 * @date: 2018年4月24日 下午3:51:46
 * @version: 1.0.0
 * @since
 * 
 */
public class DefaultThreadPoolConfig implements ThreadPoolConfig {

	/**
	 * 创建一个新的实例 DefaultThreadPoolConfig.
	 */
	public DefaultThreadPoolConfig() {
		// Nothing todo
	}

	/** 
	 * @see com.ngplat.paralleltask.task.config.ThreadPoolConfig#getTaskTimeoutMillSeconds()
	 */
	@Override
	public long getTaskTimeoutMillSeconds() {
		return TASK_TIMEOUT_MILL_SECONDS;
	}

	/** 
	 * @see com.ngplat.paralleltask.task.config.ThreadPoolConfig#getQueueFullSleepTime()
	 */
	@Override
	public int getQueueFullSleepTime() {
		return QUEUE_FULL_SLEEP_TIME;
	}

	/** 
	 * @see com.ngplat.paralleltask.task.config.ThreadPoolConfig#getMaxCacheTaskNum()
	 */
	@Override
	public int getMaxCacheTaskNum() {
		return MAX_CACHE_TASK_NUM;
	}

	/** 
	 * @see com.ngplat.paralleltask.task.config.ThreadPoolConfig#getCoreTaskNum()
	 */
	@Override
	public int getCoreTaskNum() {
		return CORE_TASK_NUM;
	}

	/** 
	 * @see com.ngplat.paralleltask.task.config.ThreadPoolConfig#getMaxTaskNum()
	 */
	@Override
	public int getMaxTaskNum() {
		return MAX_TASK_NUM;
	}

}
