/**
 * @project: parallel-task
 * @package: com.ngplat.paralleltask.test.spring
 * @filename: ExceptionTask.java
 *
 * Copyright (c) 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 */
package com.ngplat.paralleltask.test.spring;

import com.ngplat.paralleltask.annotation.TaskBean;
import com.ngplat.paralleltask.exception.TaskExecutionException;
import com.ngplat.paralleltask.task.TaskContext;
import com.ngplat.paralleltask.task.TaskProcessor;

/** 
 * @typename: ExceptionTask
 * @brief: 〈一句话功能简述〉
 * @author: KI ZCQ
 * @date: 2018年5月29日 上午11:19:43
 * @version: 1.0.0
 * @since
 * 
 */
@TaskBean(taskId = "6", name = "ExceptionTask", status="D")
public class ExceptionTask extends TaskProcessor {

	// serialVersionUID
	private static final long serialVersionUID = 8230140529703591276L;

	/**
	 * 创建一个新的实例 ExceptionTask.
	 */
	public ExceptionTask() {
		// TODO Auto-generated constructor stub
	}

	/** 
	 * @see com.ngplat.paralleltask.task.TaskProcessor#runWorker(com.ngplat.paralleltask.task.TaskContext)
	 */
	@Override
	public void runWorker(TaskContext context) throws TaskExecutionException {
		throw new TaskExecutionException("我是故意的!!!");
	}

}
