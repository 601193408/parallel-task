/**
 * @project: parallel-task
 * @package: com.ngplat.paralleltask.test.spring
 * @filename: ForkJoinTask.java
 *
 * Copyright (c) 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 */
package com.ngplat.paralleltask.test.spring;

import java.util.Map;

import com.ngplat.paralleltask.annotation.TaskBean;
import com.ngplat.paralleltask.forkjoin.ForkJoinProcessor;
import com.ngplat.paralleltask.task.TaskContext;

/** 
 * @typename: ForkJoinTask
 * @brief: 简单的加法计算
 * @author: KI ZCQ
 * @date: 2018年6月5日 上午12:46:34
 * @version: 1.0.0
 * @since
 * 
 */
@TaskBean(taskId = "2", name = "ForkJoinTask", jobId = "SECOND_JOB")
public class ForkJoinTask extends ForkJoinProcessor<Integer> {

	// serialVersionUID
	private static final long serialVersionUID = 5797526589983893221L;

	int subListSize = 1000;
	
	/**
	 * 创建一个新的实例 ForkJoinTask.
	 * @param clazz
	 */
	public ForkJoinTask() {
		// TODO worker
		super(SumBigDataTask.class);
	}
	
	/** 
	 * @see com.ngplat.paralleltask.task.TaskProcessor#beforeExecute(com.ngplat.paralleltask.task.TaskContext)
	 */
	@Override
	public void beforeExecute(TaskContext context) {
		int value = 0;
		for(int i = 0; i < 100000; i++) {
			srcList.add(i);
			value += i;
		}
		
		System.out.println("[beforeExecute] 总结果为: " + value);
	}
	
	/** 
	 * @see com.ngplat.paralleltask.task.TaskProcessor#afterExecute(com.ngplat.paralleltask.task.TaskContext)
	 */
	@Override
	public void afterExecute(TaskContext context) {
		// 结果
		Map<String, Object> result = context.getResult();
		
		int value = 0;
		for(Object ret : result.values()) {
			value += (int)ret;
		}
		
		System.out.println("[afterExecute] 总结果为: " + value);
	}

}
