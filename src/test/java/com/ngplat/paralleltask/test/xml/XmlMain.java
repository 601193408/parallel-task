/**
 * @project: parallel-task
 * @package: com.ngplat.paralleltask.test.xml
 * @filename: XmlMain.java
 *
 * Copyright (c) 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 */
package com.ngplat.paralleltask.test.xml;

import java.util.List;

import com.ngplat.paralleltask.model.TaskInfo;
import com.ngplat.paralleltask.task.TaskPoolManager;
import com.ngplat.paralleltask.utils.TaskXmlParser;
import com.ngplat.paralleltask.utils.XmlUtils;

/** 
 * @typename: XmlMain
 * @brief: 〈一句话功能简述〉
 * @author: KI ZCQ
 * @date: 2018年5月31日 上午11:32:31
 * @version: 1.0.0
 * @since
 * 
 */
public class XmlMain {
	
	public static void main(String[] args) {
		String path = XmlUtils.class.getClassLoader().getResource("xml/task_xml_template.xml").getPath();
		List<TaskInfo> taskList = TaskXmlParser.parseTaskXml(path);
		
		// add Task info
		TaskPoolManager.DEFAULT.addTasks(taskList);
		// 启动任务
		TaskPoolManager.DEFAULT.start();
	}

}
